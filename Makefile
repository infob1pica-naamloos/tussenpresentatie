all: pdf

pdf: tags directories
	pdflatex presentatie.tex

tags:
	dot -Tpng -otags.png tags.gv

directories:
	dot -Tpng -odirectories.png directories.gv
